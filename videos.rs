pub fn collection_of_video_extensions() -> Vec<String> {
    let videos = vec!["mp4".to_string(),
                        "mov".to_string(),
                        "wmv".to_string(),
                        "avi".to_string(),
                        "avchd".to_string(),
                        "flv".to_string(),
                        "f4v".to_string(),
                        "swf".to_string(),
                        "mkv".to_string(),
                        "webm".to_string(),
                        "html5".to_string(),
                        "mpeg-2".to_string()];
    return videos;
}
