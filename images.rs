pub fn collection_of_image_extensions() -> Vec<String> {
    let images = vec!["jpeg".to_string(),
                        "jpg".to_string(),
                        "png".to_string(),
                        "gif".to_string(),
                        "tiff".to_string(),
                        "psd".to_string(),
                        "eps".to_string(),
                        "ai".to_string(),
                        "indd".to_string(),
                        "raw".to_string(),
                        "svg".to_string()];
        return images;
}