pub fn collection_of_document_extensions() -> Vec<String> {
    let documents = vec!["odt".to_string(),
                        "ods".to_string(),
                        "odp".to_string(),
                        "odg".to_string(),
                        "doc".to_string(),
                        "docx".to_string(),
                        "xls".to_string(),
                        "xlsx".to_string(),
                        "ppt".to_string(),
                        "pptx".to_string(),
                        "pdf".to_string()];
        return documents;
}
