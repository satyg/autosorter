pub fn collection_of_music_extensions() -> Vec<String> {
    let music = vec!["mp3".to_string(),
                        "aac".to_string(),
                        "flac".to_string(),
                        "alac".to_string(),
                        "wav".to_string(),
                        "aiff".to_string(),
                        "dsd".to_string(),
                        "pcm".to_string()];
        return music;
}