use std::process::Command;
use std::path::Path;
use std::fs;
use std::env::var;
mod images;
mod videos;
mod archives;
mod documents;
mod music;

/*
Uncomment first variable (line 17) 'home' and set desired name of directory to be your autosorter.
If change - comment the other variable named 'home' on line 18.
*/

fn main() {
    //let home: String = "ENTER/VALID/ABSOLUTE/PATH/TO/AUTOSTORTER/HERE".to_string();
    let home: String = format!("{}/autosorter", var("HOME").unwrap());
    let destination: String = home.clone();
    let images = images::collection_of_image_extensions();
    let videos = videos::collection_of_video_extensions();
    let archives = archives::collection_of_archive_extensions();
    let documents = documents::collection_of_document_extensions();
    let music = music::collection_of_music_extensions();
    let dest_dirs = vec!["/FOLDERS", "/IMAGES", "/VIDEOS", "/ARCHIVES", "/DOCUMENTS", "/MUSIC"];

    // Iterate directories to create                     
    for name in dest_dirs.iter() {
        let mut src = format!("{}", home);
        let absolute: String = (&name).to_string();
        src.push_str(&absolute);
        let b: bool = Path::new(&src).is_dir();

        // If directory doesn't exist... create it
        if b == false {
            Command::new("mkdir")
                    .arg(&src)
                    .spawn()
                    .expect("Error...");
        }
    }

    let paths = fs::read_dir(home).unwrap();
    for path in paths {
        let file: String = path.as_ref().unwrap().path().display().to_string();
        let extension: String = get_extension_from_filename(file);

            fn get_extension_from_filename(filename: String) -> String {
                //Change it to a canonical file path.
                let path = Path::new(&filename).canonicalize().expect("Expecting an existing filename",);
                let filepath = path.to_str();
                let name = filepath.unwrap().split('/');
                let names: Vec<&str> = name.collect();
                let extension = names.last().expect("File extension can not be read.");
                let extens: Vec<&str> = extension.split(".").collect();
                return extens[1..(extens.len())].join(".").to_string();
            }
            
        let file_to_move: String = path.unwrap().path().display().to_string();
        let file_extension = extension.to_string();
        if let Some(_str) = images.iter().find(|&s| *s == file_extension) {
            Command::new("mv")
                    .args([file_to_move.to_string(), format!("{}{}", destination, dest_dirs[1])])
                    .spawn()
                    .expect("Error moving file");
        } else if let Some(_str) = videos.iter().find(|&s| *s == file_extension) {
            Command::new("mv")
                    .args([file_to_move.to_string(), format!("{}{}", destination, dest_dirs[2])])
                    .spawn()
                    .expect("Error moving file");
        } else if let Some(_str) = archives.iter().find(|&s| *s == file_extension) {
            Command::new("mv")
                    .args([file_to_move.to_string(), format!("{}{}", destination, dest_dirs[3])])
                    .spawn()
                    .expect("Error moving file");
        } else if let Some(_str) = documents.iter().find(|&s| *s == file_extension) {
            Command::new("mv")
                    .args([file_to_move.to_string(), format!("{}{}", destination, dest_dirs[4])])
                    .spawn()
                    .expect("Error moving file");
        } else if let Some(_str) = music.iter().find(|&s| *s == file_extension) {
            Command::new("mv")
                    .args([file_to_move.to_string(), format!("{}{}", destination, dest_dirs[5])])
                    .spawn()
                    .expect("Error moving file");
        }       
    }
}
